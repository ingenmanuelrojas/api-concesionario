<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Car;

class CarController extends Controller
{
    public function index(Request $request){
        $hash = $request->header('Authorization', null);

    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToten($hash);

    	if ($checkToken) {
            $cars = Car::all()->load('user');
            $data = array(
                'status' => 'success',
                'code'   => 200,
                'cars'   => $cars
            );
            return response()->json($data, 200);
    	}else{
    		$data = array(
    			'status'  => 'error',
    			'code'    => 401,
    			'message' => 'Token incorrecto.'
    		);
            return response()->json($data, 200);
    	}

    }

    public function show($id = null){

        if (Car::find($id)) {
            $car = Car::find($id)->load('user');
            $data = array(
                'status' => 'success',
                'code' => 200,
                'car' => $car
            );
        }else{
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'No existe el registro'
            );
        }
        return response()->json($data, 200);
    }

    public function store(Request $request){
        $hash = $request->header('Authorization', null);

    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToten($hash);

    	if ($checkToken) {
    		
    		$json = $request->input('json', null);
			$params = json_decode($json);
			$params_array = json_decode($json, true);

    		//Obtener el usuario identificado
    		$user = $jwtAuth->checkToten($hash, true);
    		
    		//Validacion de datos
            $validate = \Validator::make($params_array, [
                'title'       => 'required',
                'description' => 'required',
                'status'      => 'required',
                'price'       => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'  => 'error',
                    'code'    => 400,
                    'detailsError' => $validate->errors()
                );

                return response()->json($data,400);
            }

    		//Guargar Carro
    		$car = new Car();
    		$car->user_id     = $user->sub; //En Sub esta el ID del usuario autenticado por jwt
    		$car->title       = $params->title;
    		$car->description = $params->description;
    		$car->estatus     = $params->status;
    		$car->price       = $params->price;

    		$car->save();

    		$data = array(
    			'status'  => 'success',
    			'code'    => 200,
    			'car'     => $car
    		);
    	}else{
    		$data = array(
    			'status'  => 'error',
    			'code'    => 300,
    			'message' => 'Login incorrecto.'
    		);
        }
        
    	return response()->json($data,200);

    }

    public function update(Request $request, $id){
        $hash = $request->header('Authorization', null);

    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToten($hash);

    	if ($checkToken) {
            //recoger parametros post
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json,true);

            //validar datos
            $validate = \Validator::make($params_array, [
                'title'       => 'required',
                'description' => 'required',
                'estatus'      => 'required',
                'price'       => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'  => 'error',
                    'code'    => 400,
                    'detailsError' => $validate->errors()
                );

                return response()->json($data,400);
            }

            //actualizar el registro
            $car = Car::where('id',$id)->update($params_array);

            $data = array(
                'status' => 'success', 
                'code' => 200, 
                'car' => $params, 
            );

            return response()->json($data,200);
    	}else{
    		$data = array(
    			'status'  => 'error',
    			'code'    => 300,
    			'message' => 'Token incorrecto.'
    		);
            return response()->json($data, 200);
    	}
    }

    public function destroy(Request $request,$id){
        $hash = $request->header('Authorization', null);

    	$jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToten($hash);
        
        if ($checkToken) {
            $car = Car::find($id)->delete();

            $data = array(
                'status' => 'success',
                'code'   => 200,
                'message' => 'Vehiculo eliminado correctamente',
            );
            return response()->json($data, 200);
    	}else{
    		$data = array(
    			'status'  => 'error',
    			'code'    => 401,
    			'message' => 'Token incorrecto.'
    		);
            return response()->json($data, 200);
    	}
    }
}
