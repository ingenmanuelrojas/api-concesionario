<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\User;

class UserController extends Controller
{
    public function register(Request $request){
        $json   = $request->input('json', null);
        $params = json_decode($json);

        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $name = (!is_null($json) && isset($params->name)) ? $params->name : null;
        $surname = (!is_null($json) && isset($params->surname)) ? $params->surname : null;
        $role = "ROLE_USER";
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
    
        if (!is_null($email) && !is_null($name) && !is_null($surname) && !is_null($password)) {
            
            //crear el usuario
            $user = new User();
            $user->email    = $email;
            $user->name     = $name;
            $user->surname  = $surname;
            $user->role     = $role;

            $pwd = hash('sha256', $password);
            $user->password = $pwd;

            //comprobar usuario duplicado
            $isset_user = User::where('email', '=', $email)->count();
            if($isset_user == 0){
                $user->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario creado exitosamente',
                );
            }else{
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario duplicado.',
                );
            }

        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no creado',
            );
        }
        return response()->json($data,200);
    }

    public function login(Request $request){
        $jwtauth = new JwtAuth();

        //Recibir los datos por POST
        $json   = $request->input('json', null);
        $params = json_decode($json);

        $email    = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $getToken = (!is_null($json) && isset($params->getToken)) ? $params->getToken : null;
    
        //Cifrar la password
        $pwd = hash('sha256', $password);

        if (!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')) {
            $signup = $jwtauth->signup($email, $pwd);
        }elseif($getToken != null){
            $signup = $jwtauth->signup($email, $pwd, $getToken);
        }else{
            $signup = array(
                'status'  => 'error', 
                'message' => 'Envia tus datos por post'
            );
        }

        return response()->json($signup, 200);
    }
}
